# \<cookie-clicker>
A cookie-clicker game

`cd cookie-clicker`, `polymer serve` and visit `localhost/cookie-clicker/`.

## 1: Binding properties & attributes
 - Import your fancy-text element in both cookie-producer.html and cookie-button.html<br>
   If it doesn't work, import the Polymer version instead: `cookie-clicker/fancy-text-polymer.html`
 - Go to `cookie-clicker/cookie-producer.html`
 - Add your fancy-text component to the template above the loading-bar, wrapped in a counter-icon component
 - Bind the `image` property of counter-icon to the cookie-producer its `image` property
 - Bind the `amount` property to the `text` attribute in your fancy-button.
   Attributes are bound by using `$=`, e.g.: `<custom-element attr$="prop"></custom-element>`

## 2: Event handling
 - Import the Polymer element: `bower_components/paper-button/paper-button.html` and add it to your template.
 - Add the `raised` attribute for good looks
 - Add some text to the button, e.g.: "Buy 1 for X cookies"
 - Listen to button click events by adding `on-click="onClick"`

If everything is implemented correctly, you should now be able to buy NaN producers, without cost, while producing nothing

## 3: Properties
 - We are getting NaN producers, because we are incrementing the `amount` property, while `amount` is undefined.
   To fix this, we should add a default value by adding the `value` property to `amount`.
   Replace the `amount` property with `{type: Number, value: 0}`
 - Now we can buy an infinite amount of cookies for free.
   The `cookies` value is being updated by the onClick method, but it doesn't update the `cookies` value of the cookie-clicker component.
   If you look at the cookie-clicker component, you see the mustache syntax: `cookies={{cookies}}`, which is just like Angular and AngularJS a two-way-binding syntax.
   But it doesn't work, because the cookie-producer does not notify the cookie-clicker component about the changes.
   To notify the parent component, we have to add the `notify` property to the property with a value of `true`.
 - Now we should still be able to buy an infinite amount of producers, but at least we are paying for it now.
   The next step is to stop the purchase of producers when having an insufficient amount of cookies.
     - Add the computed property to the disabled property with a value of `'isDisabled(cookies)'`.
       Polymer will automatically call this method when the cookies property is updated
     - Now add the `disabled` attribute to the paper-button
 - Now we are only able to buy a producer if we have enough cookies, but the producers are still not producing anything.
   The `updateDuration` method starts this interval, and changes the interval whenever you call it again.
   We would like to call this method whenever the `duration` property is called.
   To do so, we should add an `observer` property to the `duration` property.

## 4: Polymer components
A few Polymer components are available by default when importing `polymer-element.html`.
 - One of these is the dom-repeat, which serves the function similar to `ng-repeat` in Angular.
   dom-repeat iterates over all items within a given array, and inserts a clone of the given template for every item.
   In cookie-clicker/cookie-clicker.html there is an unused property called `producers`.
    - Use dom-repeat to iterate over producers
    - Add a template with a cookie-producer, and fill all the required properties
    - Remove the original cookie-producer element from the cookie-clicker.html

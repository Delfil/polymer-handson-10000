# \<fancy-text>
Displays text in a fancy fashion, and automatically resizes the text depending on the text length.
Accepts the attribute `text`, which is the value to display

## 0: Setup
 - Create a fancy-text/fancy-text.html
 - `<!DOCTYPE html><html><head... etc`
 - `<script>`
 - `'use strict'`
 - IIFE (Immediately-Invoked Function Expression)

## 1: Create a html element
 - Create a class extending `HTMLElement`
 - Register this class using `document.registerElement`

## 2: Add template
 - Add a template to the html
 - Implement the createdCallback method:
   - Use the `document.currentScript.ownerDocument` to select the template
   - Create a new node using the template content and attach it to the `HTMLElement`

## 3: HTML import
 - Create a fancy-text/index.html and import your component
 - Add some text to your template to see if it works
 - run `polymer serve` and navigate to `localhost/fancy-text/`
 - Check your browser's develop tools to confirm if it is working

## 4: Shadow dom
 - Import a stylesheet into `fancy-text.html`
```html
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:700" rel="stylesheet">
```
 - Add the following styling to your template:
```css
* {
    text-align: center;

    font-family: 'Roboto Mono', monospace;

    -webkit-text-fill-color: white;
    -webkit-text-stroke-color: black;

    font-size: 200;
    -webkit-text-stroke-width: 12px;

    transform-origin: 50%;
}

@keyframes score-animation {
    30% {
        transform: scale(2);
    }
    100% {
        transform: scale(1);
    }
}

.animation {
    animation: score-animation 0.3s ease-out;
}
```
 - Add some text to the `index.html`, and see how the style of the hole page is affected,
 instead of only the fancy-text component
 - Create a new shadow root using `attachShadow({mode: 'open'})`
 - Add the template to the shadowRoot instead of the element itself

## 5: Static binding - Display the value of the text attribute
 - Add a div to the template
 - In the createdCallback, obtain the value of the text attribute
 - Select the div and save it in this.textElement
 - Update the textContent of the div to be the text attribute value

## 6: Dynamic binding
 - Add the following methods to the fancy-text component:
```js
updateFont(newValue) {
    // Compute font size
    let divider = newValue.length + 1;
    let newFontSize = 200 / divider;
    let newStrokeWidth = 12 / divider;

    // Update style
    this.textElement.style.fontSize = newFontSize + 'px';
    this.textElement.style.webkitTextStrokeWidth = newStrokeWidth + 'px';
}

animate() {
    this.textElement.classList.remove('animation');

    // Magic
    void this.textElement.offsetWidth;

    this.textElement.classList.add('animation');
}
```
 - Implement the attributeChangedCallback(name, oldValue, newValue) method:
   When the `text` attribute is changed:
     - Update the div's `textContent`
     - Call `updateFont`
     - Call `animate`
 - Update the createdCallback to call `updateFont` and `animate`
